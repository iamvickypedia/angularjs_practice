var gulp = require('gulp');
var gutil = require('gulp-util');
var webserver = require('gulp-webserver');
var browserSync = require('browser-sync');

var reload = browserSync.reload;



gulp.task('copy', function() {
  gulp.src('app/*')
  .pipe(gulp.dest('prod'))
});

gulp.task('log', function() {
  gutil.log('== My Log Task ==')
});

// gulp.task('connect', function() {
//   connect.server({
//     root: 'app/',
//     livereload: true
//   })
// });
gulp.task('serve', function() {
  gulp.src('app')
  .pipe(webserver({
    fallback:   'index.html',
    livereload: true,
    directoryListing: false,    
    open: true,
    port: 3000
  }));
    gulp.watch(['*.html', 'styles/*.css', 'scripts/**/*.js'], {cwd: 'app'}, reload);
});

gulp.task('default',['log','serve'])